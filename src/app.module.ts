import { Module } from '@nestjs/common';
import { MainGateway } from './gateways/main.gateway';
import { MongooseModule } from '@nestjs/mongoose';
import { MONGO_URI } from './config';
import { DeviceSchema } from './models/device.model';
import { UserSchema } from './models/user.model';
import { APP_GUARD } from '@nestjs/core';
import { AuthGuard } from './guards/auth.guard';
import { UsersModule } from './modules/users/users.module';

const MongooseConnection = MongooseModule.forRoot(MONGO_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
});

@Module({
  imports: [
    MongooseConnection,
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    MongooseModule.forFeature([{ name: 'Device', schema: DeviceSchema }]),
    UsersModule
  ],
  controllers: [],
  providers: [{ provide: APP_GUARD, useClass: AuthGuard }, MainGateway],
})
export class AppModule {
}
