import { ApiResponseProperty } from "@nestjs/swagger";

export class BaseSchema {
  @ApiResponseProperty()
  // tslint:disable-next-line: variable-name
  _id: string;

  @ApiResponseProperty()
  createdAt?: Date;

  @ApiResponseProperty()
  updatedAt?: Date;
}
