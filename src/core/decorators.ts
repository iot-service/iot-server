import { User } from 'src/models/user.model';
import { createParamDecorator } from '@nestjs/common';
import { Request } from 'express';

export const UserData = createParamDecorator(
  (data, request: Request): User => (request as any).user as User
);