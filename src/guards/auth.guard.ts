import { applyDecorators, CanActivate, ExecutionContext, Injectable, SetMetadata } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { InjectModel } from '@nestjs/mongoose';
import { ApiBearerAuth } from '@nestjs/swagger';
import { Request } from 'express';
import * as jwt from 'jsonwebtoken';
import { Model } from 'mongoose';
import { JWT_SECRET } from 'src/config';
import { User, UserModel } from 'src/models/user.model';

export const Auth = (...roles: string[]) =>
  applyDecorators(
    SetMetadata('auth', true),
    SetMetadata('roles', roles),
    ApiBearerAuth(),
  );

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    @InjectModel('User') private readonly userModel: Model<UserModel>,
    private readonly reflector: Reflector,
  ) {
  }

  getUser = (decoded: any) =>
    this.userModel.findById(decoded._id).select('-keys -password');

  async canActivate(context: ExecutionContext): Promise<boolean> {
    try {
      const request: Request = context.switchToHttp().getRequest();
      const auth = this.reflector.get<boolean>('auth', context.getHandler());
      const roles = this.reflector.get<string[]>('roles', context.getHandler());

      if (!auth) {
        return true;
      }

      const { authorization } = request.headers;

      if (!!authorization) {
        const token = authorization.split(' ')[1];
        const decoded = jwt.verify(token, JWT_SECRET) as any;
        if (!!decoded) {
          const user: User = await this.getUser(decoded);
          (request as any).user = user;
          if (!roles || roles.length === 0) {
            return true;
          } else {
            return roles.indexOf(user.role) !== -1;
          }
        } else {
          return false;
        }
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }
}
