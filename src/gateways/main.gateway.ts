import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { DeviceModel } from '../models/device.model';
import { User, UserModel } from '../models/user.model';
import * as jwt from 'jsonwebtoken';
import { Logger } from '@nestjs/common';

@WebSocketGateway({ transports: ['websocket'] })
export class MainGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {

  logger: Logger = new Logger('MainGateway');

  constructor(
    @InjectModel('User') private readonly userModel: Model<UserModel>,
    @InjectModel('Device') private readonly deviceModel: Model<DeviceModel>,
  ) {
  }

  afterInit(server: any): any {
    this.logger.log('Web Socket Initialized.');
  }

  handleConnection(client: Socket, payload: any) {
    this.logger.log('connect    :', client.id);
  }

  async handleDisconnect(client: Socket) {
    this.logger.log('disconnect :', client.id);

    const isDevice = await this.deviceModel.exists({ socketID: client.id });
    const isUser = await this.userModel.exists({ socketID: client.id });

    if (isDevice) {
      await this.deviceModel.findOneAndUpdate(
        { socketID: client.id },
        { isOnline: false },
        { new: true, upsert: false },
      );
    } else if (isUser) {
      await this.userModel.findOneAndUpdate(
        { socketID: client.id },
        { isOnline: false },
        { new: true, upsert: false },
      );
    }

  }

  @SubscribeMessage('device:register')
  async deviceRegister(client: Socket, payload: { device: string }) {
    await this.deviceModel.findOneAndUpdate(
      { device: payload.device },
      { device: payload.device, socketID: client.id, isOnline: true },
      { new: true, upsert: true });
  }

  @SubscribeMessage('device:litre')
  async deviceLitre(client: Socket, payload: { device: string, litre: number }) {
    const device = await this.deviceModel
      .findOneAndUpdate(
        { device: payload.device },
        { device: payload.device, litre: payload.litre },
        { new: true, upsert: false },
      )
      .populate('users')
      .exec();

    const usersSocketID = device.users
      .filter((user: User) => user.isOnline)
      .map((user: User) => user.socketID);

    usersSocketID.forEach(id => {
      client.to(id).emit('device:litre', payload);
    });

  }

  @SubscribeMessage('device:control')
  async deviceControl(client: Socket, payload: { device: string, value: string }) {
    const device = await this.deviceModel.findOne({ device: payload.device });
    if (device.isOnline) {
      client.to(device.id).emit('control', { value: payload.value });
    } else {
      client.emit('error', { message: 'دستگاه آنلاین نیست !' });
    }
  }

  @SubscribeMessage('user:register')
  async userRegister(client: Socket, payload: { token: string }) {
    const { id } = jwt.decode(payload.token) as { id: string };
    await this.userModel.findByIdAndUpdate(
      id,
      { socketID: client.id, isOnline: true },
      { new: true, upsert: false });
  }
}
