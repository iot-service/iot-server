import { arrayProp, buildSchema, prop, Ref } from '@typegoose/typegoose';
import { Document } from 'mongoose';
import { BaseSchema } from 'src/core/baseSchema';
import { ApiProperty } from '@nestjs/swagger';
import Validator from 'validator';
import { User } from './user.model';

export class Device extends BaseSchema {
  @ApiProperty()
  @arrayProp({ ref: 'User' })
  users: Ref<User>[];

  @ApiProperty()
  @prop({ required: true })
  device?: string;

  @ApiProperty()
  @prop({ required: true })
  socketID?: string;

  @ApiProperty()
  @prop({ required: true })
  litre?: number;

  @ApiProperty()
  @prop()
  isOnline?: boolean;
}

export const DeviceSchema = buildSchema(Device, { timestamps: true });
export type DeviceModel = Device & Document;
