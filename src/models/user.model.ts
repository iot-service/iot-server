import { ApiProperty } from '@nestjs/swagger';
import { arrayProp, buildSchema, prop } from '@typegoose/typegoose';
import * as md5 from 'md5';
import { Document } from 'mongoose';
import { BaseSchema } from 'src/core/baseSchema';

const USER_STATUS = ['active', 'inactive', 'block'];
const USER_ROLE = ['admin', 'user'];

class UserKeys {
  @prop()
  activateKey?: string;
  @prop()
  activateExpire?: Date;
  @prop()
  resetPasswordKey?: string;
  @prop()
  resetPasswordExpire?: Date;
}

export class User extends BaseSchema {
  @ApiProperty({ required: true })
  @prop({ required: true, unique: true })
  phone: string;

  @ApiProperty({ required: false })
  @prop()
  name?: string;

  @ApiProperty({ required: false })
  @prop()
  avatar?: string;

  @ApiProperty()
  @prop({ required: true })
  socketID?: string;

  @ApiProperty()
  @prop()
  isOnline?: boolean;

  @ApiProperty({
    required: false,
    readOnly: true,
    enum: USER_STATUS,
    default: 'inactive',
  })
  @prop({ type: String, enum: USER_STATUS, default: 'inactive' })
  status?: string;

  @ApiProperty({
    required: false,
    readOnly: true,
    enum: USER_ROLE,
    default: 'user',
  })
  @prop({ type: String, enum: USER_ROLE, default: 'user' })
  role?: string;

  @prop()
  keys: UserKeys;
}

async function preSave(next) {
  if (this.isModified('password')) {
    this.password = md5(this.password);
  }
  return next();
}

export const UserSchema = buildSchema(User, { timestamps: true })
  .pre('save', preSave);

export type UserModel = User & Document;
