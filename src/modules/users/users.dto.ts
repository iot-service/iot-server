import { User } from "src/models/user.model";
import { ApiProperty } from "@nestjs/swagger";

export class UserResponseDto {
  @ApiProperty()
  user: User;
}
