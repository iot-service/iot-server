import { Module } from "@nestjs/common";
import { UsersController } from "./users.controller";
import { UserSchema } from "src/models/user.model";
import { MongooseModule } from "@nestjs/mongoose";
import { SmsService } from "src/services/sms.service";

@Module({
  imports: [MongooseModule.forFeature([{ name: "User", schema: UserSchema }])],
  controllers: [UsersController],
  providers: [SmsService]
})
export class UsersModule {}
