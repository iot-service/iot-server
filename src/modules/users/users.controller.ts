import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Post,
  Query,
  Put,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  ApiBody,
  ApiOkResponse,
  ApiOperation, ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import * as jwt from 'jsonwebtoken';
import { Model } from 'mongoose';
import * as randomstring from 'randomstring';
import { JWT_SECRET } from 'src/config';
import { UserData } from 'src/core/decorators';
import { Auth } from 'src/guards/auth.guard';
import { User, UserModel } from 'src/models/user.model';
import { SmsService } from 'src/services/sms.service';
import { UserResponseDto } from './users.dto';

@ApiTags('Users')
@Controller('api/users')
export class UsersController {
  constructor(
    @InjectModel('User') private readonly userModel: Model<UserModel>,
    private smsService: SmsService,
  ) {
  }

  //#region Route: Post /users/signin
  @ApiOperation({ summary: 'ورود کاربر' })
  @ApiBody({
    schema: {
      properties: { phone: { type: 'string' } },
    },
  })
  @ApiOkResponse({ schema: { properties: { status: { type: 'boolean' } } } })
  @Post('signin')
  async signin(@Body('phone') phone: string): Promise<{ status: boolean }> {
    try {
      const userExist = await this.userModel.exists({ phone });

      const keys = {
        activateKey: randomstring.generate({
          charset: 'numeric',
          length: 6,
        }),
        activateExpire: new Date(Date.now() + 60 * 60 * 1000),
      };

      if (userExist) {
        await this.userModel.findOneAndUpdate(
          { phone },
          { keys },
          { new: true },
        );
      } else {
        const user = new this.userModel({ phone, keys });
        await user.save();
      }

      const activationKey = keys.activateKey;
      // TODO: Send SMS
      this.smsService.send(phone, activationKey);

      return { status: true };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  //#endregion
  //#region Route: Post /users/verify
  @ApiOperation({ summary: 'تایید کاربر' })
  @ApiBody({
    schema: {
      properties: { phone: { type: 'string' }, key: { type: 'string' } },
    },
  })
  @ApiResponse({
    schema: {
      properties: {
        message: { type: 'string' },
        token: { type: 'string', description: 'توکن' },
        id: { type: 'string', description: 'آی دی کاربر' },
      },
    },
  })
  @Post('verify')
  async verify(@Body('phone') phone: string, @Body('key') key: string) {
    try {
      const user = await this.userModel.findOne({
        phone,
        'keys.activateKey': key,
      });

      if (user) {
        if (user.keys.activateExpire <= new Date()) {
          throw { message: 'درخواست شما منقضی شده است .' };
        }

        user.keys.activateKey = '';
        user.keys.activateExpire = new Date(Date.now());
        user.status = 'active';
        await user.save();

        const payload = { _id: user._id };
        const token = jwt.sign(payload, JWT_SECRET);

        return { message: 'شما وارد شدید !', token, id: user._id };
      } else {
        throw { message: 'کد فعال سازی اشتباه است .' };
      }
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  //#endregion
  //#region Route: Get /users
  @ApiOperation({ summary: 'لیست کاربران' })
  @ApiQuery({ name: 'search', type: String })
  @ApiQuery({ name: 'skip', type: Number })
  @ApiQuery({ name: 'limit', type: Number })
  @ApiOkResponse({ type: [User] })
  @Auth('admin')
  @Get()
  async getUsersList(
    @Query('search') search: string,
    @Query('skip') skip: number = 0,
    @Query('limit') limit: number = 10,
  ): Promise<User[]> {
    try {
      const query = {};
      if (!!search) {
        Object.assign(query, { name: new RegExp(search, 'ig') });
      }

      const users = await this.userModel
        .find(query)
        .skip(Number(skip.toString()))
        .limit(Number(limit.toString()));

      return users;
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  //#endregion
  //#region Route: Get /users/profile
  @ApiOperation({ summary: 'دریافت پروفایل' })
  @ApiOkResponse({ type: UserResponseDto })
  @Auth()
  @Get('profile')
  async profile(@UserData() userData: User): Promise<UserResponseDto> {
    try {
      const user = await this.userModel.findById(userData, '-keys -status');
      return { user };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  //#endregion
  //#region Route: Put /users/Profile
  @ApiOperation({ summary: 'بروزرسانی پروفایل کاربر' })
  @ApiBody({ type: User })
  @ApiOkResponse({ type: User })
  @Auth()
  @Put('profile')
  async updateUser(
    @UserData() userdata: User,
    @Body() data: User,
  ): Promise<UserResponseDto> {
    try {
      const user = await this.userModel.findByIdAndUpdate(userdata._id, data, {
        new: true,
      });
      return { user };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  //#endregion
  //#region Route: Post /users
  @Auth('admin')
  @ApiOkResponse({ type: UserResponseDto })
  @Post()
  async createUser(@Body() data: User): Promise<UserResponseDto> {
    try {
      const user = new this.userModel(data);
      await user.save();
      return { user };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  //#endregion

}
